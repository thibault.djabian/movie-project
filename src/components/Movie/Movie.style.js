import styled from '@emotion/styled';
import { css } from '@emotion/core';  

export const Fiche = styled.section`
   display: flex;
   flex-direction: row;
   justify-content: space-evenly;
   align-items: center;
   width: 100%;
   box-shadow: 0px 0px 10px 1px rgba(119, 119, 119, 0.75);
    -moz-box-shadow: 0px 0px 10px 1px rgba(119, 119, 119, 0.75);
    -webkit-box-shadow: 0px 0px 10px 1px rgba(119, 119, 119, 0.75);
    border-radius: 10px;
    padding-bottom: 25px;
`;

export const Timg = styled.section`
   display: flex;
   flex-direction: column;
   align-items: center;
`;

export const Type = styled.a`
   font-weight: bold;
   font-size: 1.4em;
   margin: 20px;
`;

export const Desc = styled.div`
   margin: 20px;
   align-items: right;
`;

export const Complet = styled.div`
   align-items: right;
`;

export const H1 = styled.h1`
   font-size: 2.5em;
   color: rgb(123, 0, 0);
`;