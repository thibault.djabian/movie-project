import React from 'react';
import { Fiche, Timg, Type, Desc, Complet, H1 } from './Movie.style';


const Movie = ({ movie }) => (
    <Fiche>
        <Timg>
            <H1>{movie.Title}</H1>
            <div>
                <img src={movie.Poster}/>
            </div>
        </Timg>
        <Complet>
            <Desc>
                <Type>Acteur principaux: </Type>{movie.Actors}
            </Desc>
            <Desc>
                <Type>Genre: </Type>{movie.Genre}
            </Desc>
            <Desc>
                <Type>Description: </Type>{movie.Plot}
            </Desc>
            <Desc>
               <Type>Pays de production: </Type>{movie.Country}
            </Desc>
            <Desc>
                <Type>Durée du Film: </Type>{movie.Runtime}
            </Desc>
            <Desc>
                <Type>Date de sortie: </Type>{movie.Year}
            </Desc>
        </Complet>
    </Fiche>
  );


export default Movie;