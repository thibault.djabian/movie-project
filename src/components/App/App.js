import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import HomeContainer from '../Home/HomeContainer';
import MovieContainer from '../Movie/MovieContainer';
import { Nav } from './App.style';

const App = () => (
  <Router>
    <div className="container">
      <Nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
        </ul>
      </Nav>
      <div className="App">
        <Route exact path="/" component={HomeContainer} />
        <Route exact path="/movie/:id" component={MovieContainer} />
      </div>
    </div>
  </Router>
);

export default App;
