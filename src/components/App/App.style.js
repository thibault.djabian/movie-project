import styled from '@emotion/styled';
import { css } from '@emotion/core';  

export const Nav = styled.nav`
    list-style-type: none;
    margin: 0;
    padding: 0;
`;
