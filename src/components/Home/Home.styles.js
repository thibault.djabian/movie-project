import styled from '@emotion/styled';
import { css } from '@emotion/core';  
import {Link} from 'react-router-dom';


/*export const Title = styled.h2`
    font-family : sans-serif; 
    font-weight : bold; 
    ${props => props.color && css`
        color: ${props.color};
    `}
`;*/
export const Liste = styled.ul`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-items: center;
`;
export const Film = styled.li`
    padding: 25px;
    
    list-style-type: none;
`;
  
export const Button = styled.button`
    padding:6px 0 6px 0;
	font:Bold 13px Arial;
	background:#d34836;
	color:#fff;
	width:90px;
	border-radius:2px;
    border:none;
`;

export const Form = styled.form `
    background-color: white;
    background-position: 10px 10px; 
    background-repeat: no-repeat;
    padding-left: 40px;
  `;

export const H1 = styled.h1`
    font-size: 4em;
`;
export const H5 = styled.h5`
    
`;

export const Main = styled.section`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const StyledLink = styled(Link)`
    display: flex;
    flex-direction: column;
    align-items: center;
    text-decoration: none;
`;

export const Img = styled.img`
    margin-bottom: 15px;
    :hover {
		transform: scale(1.2);
	}
`;
export const H3 = styled.h3`
    
`;