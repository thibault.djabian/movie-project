import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { StyledLink, Liste, Film, Button, H1, H5, Main, Img, H3 } from './Home.styles';

const Home = ({ searchInput, submitValue, movies }) => (
    <Main>
      <H1>Movie Film</H1>
      <H5>Dernier Site de streaming en ligne</H5>
      <form onSubmit={submitValue}>
        <input ref={searchInput} />
        <Button type="submit">Rechercher</Button>
      </form>
      {movies.length > 0 && (
        <Liste>
          {movies.map(movie => (
            <Film key={movie.imdbID}>
              <StyledLink to={`/movie/${movie.imdbID}`}>
              <Img src={movie.Poster} />
                <H3>{movie.Title}</H3>
              </StyledLink>
            </Film>
          ))}
        </Liste>
      )}
    </Main>
);

Home.propTypes = {
  searchInput: PropTypes.object.isRequired,
  submitValue: PropTypes.func.isRequired,
};

export default Home;